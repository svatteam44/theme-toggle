import { Injectable } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';

@Injectable()
export class Globals {

  darkMode: Boolean = JSON.parse(localStorage.getItem('darkMode') || 'false');
  
  constructor(
    private overlayContainer: OverlayContainer
  ) {
    overlayContainer.getContainerElement().classList.add('dark');
  }
  
  toggleTheme() {
    this.darkMode = !this.darkMode;
    localStorage.setItem('darkMode', this.darkMode.toString());
  }

}