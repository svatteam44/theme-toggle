# ThemeToggle

This is an example of how to create a custom theme using [Angular](https://angular.io/) and [Angular Material](https://material.angular.io/). Documentation on Theming your Angular Material app can be found in this [guide](https://material.angular.io/guide/theming)


## Dark Theme

![dark](./dark.png)

## Light Theme

![light](./light.png)

---

## Usage

In order to run this project you need 
- [nodejs](https://nodejs.org/en/) 
- [angular-cli](https://angular.io/cli)

### Clone the project

```sh
git clone git@gitlab.com:svatteam44/theme-toggle.git
```

### CD into project directory

```sh
cd theme-toggle
```

### Start

```sh
npm i
npm start
```

### Open your browser to [http://localhost:4200](http://localhost:4200)

---

## If this helps you please **star** this project

